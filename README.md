## FutureHAUS Web Application

Users can navigate to the respective web page for each room in the FutureHAUS.
They can visualize the PI Vision dashboard for each room and control the devices 
using buttons by publishing appropriate MQTT messages to the MQTT server. 

---
* Web Application
	* static
		* js
			* app_test.js
			* jquery.js
		* style
			* test.css
	* templates
		* includes
			* _navbar.html
		* app_home.html
		* app_room.html	
	* app.py
	* README.md
	* room_data.py	
---
* Clear the browser's cache every time after editing the javascript.

* Topics and Points for the FutureHAUS: https://docs.google.com/document/d/1HIjwraQCne3SLiWzQO6IPSO4pmODAfiXj647VM4amHA/edit

* Final documentation regarding OSISoft PI System, server information: https://docs.google.com/document/d/1RNe_dKui0KxUz_N-J-nRJ5grmW425WRsyOTrS6uRxaI/edit

* Configuring PI Vision dashboard for the rooms:
	* After creating the dashboards, click on the gear icon to Edit Display Settings. 
	* Select share with all users to allow access to the dashboards from other machines in the same network.

* Guest access to the webApp.

	    Username - GuestUser; password: GuestPassword76

* Steps for adding guest user for the WebApp to be accessible to other machines:
    * Create new windows user (GuestUser) => add to PI Vision user group by navigating to “ lusrmgr.msc” => 
Groups => PI Vision users => Add => newly created guest user account.

* To access the FutureHAUS webApp
	* From local machine - 127.0.0.1:5000
	* From another machine on same network - http://(machineName):5000
	* Steps for logging in from another machine on same network:
	    * Login using the same network.

        * got to- https://(machinename)(where the pi vision is hosted)

        * Click on “Advanced”

        * Click “Add Exception” 

        * Click “Confirm Security Exception”

        * Go to - https://(machineName)/PiVision 
        
        * login using guest user name & password.

---
* Run **app.py** to start the web application 

(In SDME01 (Osisoft) - go to ""C:\Users\Administrator\Desktop\FutureHAUS_WebApp\futurehaus_webpage")

* **room_data.py** stores the information related to the devices in each room.
* Currently Mosquitto MQTT server is running on the SDME01 (Osisoft) machine, the
application also runs on port 5000.
* socket-io is used to create a connection between web server and flask-mqtt client.
* Buttons are used to control the devices. To send commands to the devices using this topic hierarchy: 
	* Topic: FutureHAUS/[Room]/[Thing]/[Device]/Command
	* Payload:  'Value' : TurnOn / Open
* Buttons are subscribed to the following topic hierarchy to reflect the status of the devices.
	The devices would send their status to this topic.
	* Topic: FutureHAUS/[Room]/[Thing]/[Device]
	* Payload:  'Value' : On / Opened

---
## Setting up PI Vision
### To embed a PI Vision display within an existing webpage, the display can be brought as an iFrame.

* Example
```http
<iframe src="https://futurehaus/PIVision/#/Displays/52/Test_2?mode=kiosk&HideToolbar&HideTimebar" width = 800 height = 800 >
  <p>Your browser does not support iframes.</p>
</iframe>
```

---

## How to configure for adding IFrame

navigate to web.config, in C:\Program Files\PIPC\PIVision,and search for http protocol

```http
<httpProtocol>
      <customHeaders>
        <clear />
        <add name="X-Frame-Options" value="SAMEORIGIN" />
        <add name="X-Content-Type-Options" value="nosniff" />
        <add name="Referrer-Policy" value="no-referrer" />
        <add name="X-XSS-Protection" value="1; mode=block" />
      </customHeaders>
    </httpProtocol>
```

To get the IFrame work:

change

```http
<add name="X-Frame-Options" value="SAMEORIGIN" />
```

To

```http
<add name="X-Frame-Options" value="Allow" />
```

Follow the [link](https://pisquare.osisoft.com/thread/33477-does-pi-coresightpi-vision-have-the-ability-to-support-embedded-windows) for more detail

---
## How to get just the trend and not the whole page? (Check this [link](https://livelibrary.osisoft.com/LiveLibrary/content/en/vision-v1/GUID-C643F092-EB07-41EC-8DC8-5981BF2692F4))

### Rules for adding a URL Parameter: (check this [link](https://pisquare.osisoft.com/videos/2408-add-navigation-links-and-url-parameters))

Rule 1: Separate query string parameters from the preceding base URL with a question mark (?).

Rule 2: Separate each query string parameter with an ampersand (&).

Below are some commonly used URL parameters:

#### 1. Specify kiosk mode to open a display with limited interactivity.

```http
Mode=Kiosk
```

Example:

```http
http://PISRV1PIVision/#/Displays/339/MyDisplay?mode=kiosk
```

#### 2. Specify the start and end time of the display. Any valid PI Time format is acceptable.

```http
StartTime=<PI Time> and EndTime=<PI Time>
```

Example:

```http
http://PISRV1/PIVision/#/Displays/339/MyDisplay?StartTime=*-1h&EndTime=*
```

#### 3. Hide the toolbar or toolbar from the display

```http 
HideToolbar and HideTimebar
```

Example:
```http
http://PISRV1/PIVision/#/Displays/339/MyDisplay?HideToolbar

http://PISRV1/PIVision/#/Displays/339/MyDisplay?HideTimebar
```

#### 4. You could combine these parameters with other URL parameters. Example:

```http
http://PISRV1/PIVision/#/Displays/339/MyDisplay?mode=kiosk&HideToolbar&HideTimebar
```
