"""
This class is created using the following example to include analytics in the webpage
"https://facebook.github.io/prophet/docs/quick_start.html".
The aim is to integrate FutureHAUS data with it later.

The input to Prophet is always a dataframe with two columns: ds and y. The ds (datestamp) column should be of a format
expected by Pandas, ideally YYYY-MM-DD for a date or YYYY-MM-DD HH:MM:SS for a timestamp. The y column must be numeric,
and represents the measurement we wish to forecast.
"""

__author__ = "Archi Dasgupta"
__copyright__ = "Copyright 2018"

import pandas as pd
from fbprophet import Prophet
import matplotlib.pyplot as plt

# creating dataframe
df = pd.read_csv('datasets/example_wp_log_peyton_manning.csv')

# need to create the correct format for the following dataset
#df = pd.read_csv('datasets/RefBldgHospitalNew2004_7.1_5.0_3C_USA_CA_SAN_FRANCISCO.csv')

# help(Prophet.fit)

print(df.head(5))

print("dataset shape: ")
print(df.shape)

m = Prophet()
m.fit(df)

future = m.make_future_dataframe(periods=365)
print(future.tail())

forecast = m.predict(future)
print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail())

# Plot the forecast
# Plots the observed values of our time series (the black dots),
# the forecasted values (blue line) and the uncertainty intervals
# of our forecasts (the blue shaded regions).
fig1 = m.plot(forecast, uncertainty=True)
#fig1 = forecast.plot(x='ds', y='yhat', style='.-')

plt.show(fig1)

fig2 = m.plot_components(forecast)

plt.show(fig2)




