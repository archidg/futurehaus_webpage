"""
FutureHAUS web application to view PI Vision dashboard for each
room, manage devices and look at further analysis.
This web application is built on Python Flask framework, with Javascript(JS) and HTML frontend.
"""

__author__ = "Archi Dasgupta"
__copyright__ = "Copyright 2018"

import eventlet
import json
import flask
from flask import Flask, render_template, url_for
from flask_mqtt import Mqtt
from flask_socketio import SocketIO
from flask_bootstrap import Bootstrap
from room_data import Rooms
from room_data import Devices
from room_data import PiVisionLinks
from room_data import ButtonName

import pandas as pd
from fbprophet import Prophet
import matplotlib.pyplot as plt

# For showing the plots
import io
import base64


eventlet.monkey_patch()

app = Flask(__name__)
app.config['SECRET'] = 'my secret key'
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['MQTT_BROKER_URL'] = ''   
app.config['MQTT_BROKER_PORT'] = 1883
app.config['MQTT_CLIENT_ID'] = 'futurehaus_client'
app.config['MQTT_USERNAME'] = ''
app.config['MQTT_PASSWORD'] = ''
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
app.config['MQTT_LAST_WILL_TOPIC'] = 'home/lastwill'
app.config['MQTT_LAST_WILL_MESSAGE'] = 'bye'
app.config['MQTT_LAST_WILL_QOS'] = 2

# Parameters for SSL enabled
# app.config['MQTT_BROKER_PORT'] = 8883
# app.config['MQTT_TLS_ENABLED'] = True
# app.config['MQTT_TLS_INSECURE'] = True
# app.config['MQTT_TLS_CA_CERTS'] = 'ca.crt'

# Importing data from room_data.py
Rooms = Rooms()
Devices = Devices()

# Unique PI Vision url for each room
PiVisionLinks = PiVisionLinks()
# Unique button names for devices
ButtonNames = ButtonName()

mqtt = Mqtt(app)
socketio = SocketIO(app)
bootstrap = Bootstrap(app)

# This function will be triggered when user navigates to the home page
@app.route("/")
def homepage():
    return render_template("app_home.html", rooms=Rooms)

#********************************************************************************************
# For showing the plots  -- disabling for now
# @app.route('/plot')
# def build_plot():
#
#     # creating dataframe
#     df = pd.read_csv('datasets/example_wp_log_peyton_manning.csv')
#     m = Prophet()
#     m.fit(df)
#
#     future = m.make_future_dataframe(periods=365)
#     forecast = m.predict(future)
#     print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail())
#     # Plot the forecast
#     img = io.BytesIO()
#
#     # Plots the observed values of our time series (the black dots),
#     # the forecasted values (blue line) and the uncertainty intervals
#     # of our forecasts (the blue shaded regions).
#     # fig1 = m.plot(forecast)
#     fig1 = forecast.plot(x='ds', y='yhat', style='.-')
#     #plt.show(fig1)
#
#     # Forecast components: Trend, yearly seasonality, weekly seasonality
#     # of the time series. If holidays/special days are included that would
#     # also be seen.
#     #fig2 = m.plot_components(forecast)
#     fig2 = m.plot_components(forecast)
#
#     plt.savefig(img, format='png')
#     img.seek(0)
#
#     plot_url = base64.b64encode(img.getvalue()).decode()
#     return '<img src="data:image/png;base64,{}">'.format(plot_url)
#
#     ##########
#     #img = io.BytesIO()
#
#     #y = [1,2,3,4,5]
#     #x = [0,2,1,3,4]
#     #plt.plot(x,y)
#
#     #plt.savefig(img, format='png')
#     #img.seek(0)
#
#     #plot_url = base64.b64encode(img.getvalue()).decode()
#     #return '<img src="data:image/png;base64,{}">'.format(plot_url)
#********************************************************************************************

# This function will be triggered when user navigates to the corresponding  pages for each room
@app.route("/room/<string:room_name>/")
def room(room_name):
    return render_template("app_room.html",
                           room_name=room_name,
                           devices=Devices[room_name],
                           piVisionLink=PiVisionLinks[room_name],
                           buttonName=ButtonName())

# This function will be triggered when user navigates to the sample page
# @app.route('/sample')
# def index():
#     return render_template('app_index.html')


# Communication between client and server.
# This function is called if a publish message is received via socket-io from web server.
# It publishes messages to MQTT broker.
@socketio.on('publish')
def handle_publish(json_str):
    data = json.loads(json_str)
    print('On button click: topic- ' + data['topic'] + ', Message: ' + data['message'])
    mqtt.publish(data['topic'], data['message'], data['qos'])


def ack():
    print("Connected!")

# This function is used to Verifying connection with socket-io.
@socketio.on('connect')
def on_connect():
    ack()


@socketio.on_error()
def handle_error(e):
    print(e)

@socketio.on_error_default
def handle_error_default(e):
    print(e)

#*************************************************
# publish mqtt message to the broker to control devices
# @socketio.on('analyze')
# def handle_analyze():
#     # creating dataframe
#     df = pd.read_csv('datasets/example_wp_log_peyton_manning.csv')
#
#     # need to create the correct format for the following dataset
#     # df = pd.read_csv('datasets/RefBldgHospitalNew2004_7.1_5.0_3C_USA_CA_SAN_FRANCISCO.csv')
#
#     print(df.head(5))
#
#     print("dataset shape: ")
#     print(df.shape)
#
#     m = Prophet()
#     m.fit(df)
#
#     future = m.make_future_dataframe(periods=365)
#     print(future.tail())
#
#     forecast = m.predict(future)
#     print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail())
#
#     # Plot the forecast
#     fig1 = m.plot(forecast)
#     plt.show(fig1)

#     plt.savefig('C:/Users/Administrator/PycharmProjects/Flask_Tutorial_Webpage/static/images/filenameXYZ.png')  # save to the images directory
#     return flask.jsonify({
#                              "result": "<div class='user_avatar' style='background-image:url('C:/Users/Administrator/"
#                                        "PycharmProjects/Flask_Tutorial_Webpage/static/images/figure.png');"
#                                        "width:240px;height:240px;background-size:cover;border-radius:10px;'>"})
#
#*************************************************

@socketio.on('subscribe')
def handle_subscribe(json_str):
    print("here")
    data = json.loads(json_str)
    mqtt.subscribe(data['topic'], data['qos'])


# @mqtt.on_connect()

#*****************************************************

# Communication between client and server.
# This function is called if a MQTT message is received by flask-mqtt client.
# It emits the message to web server using socket-io.
@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    print('---------Received MQTT message: '.format(), message.payload.decode())
#    print('Received MQTT topic: '.format(), message.topic)
    data = dict(
        topic=message.topic,
        payload=message.payload.decode(),
        qos=message.qos,
    )
    socketio.emit('mqtt_message', data=data)


@mqtt.on_log()
def handle_logging(client, userdata, level, buf):
    # print(level, buf)
    pass


if __name__ == '__main__':
    # the application subscribes to topics with 4 parts because it
    # does not need to listen to its own message on button click which is
    # five parts ('FutureHAUS/+/+/+/Command').
    mqtt.subscribe('FutureHAUS/+/+/+', qos=1)
    print('subscribed to FutureHAUS/+/+/+. ')
    socketio.run(app, host='0.0.0.0', port=5000, debug=True)
