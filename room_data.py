"""
This script contains data about rooms and devices
classes create dictionary with rooms and devices information
"""

__author__ = "Archi Dasgupta"
__copyright__ = "Copyright 2018"

# list of rooms and corresponding devices
def Rooms():
    rooms = [
        {
            'id': 1,
            'name': 'Lobby',
            'devices': ['Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Door/Hatch', 'Light/Total',
                        'Temperature/Total', 'Humidity/Total', 'Power/Total'],

        },
        {
            'id': 2,
            'name': 'Washroom',
            'devices': ['Door/Interior', 'Sink/Height', 'Toilet/Height', 'Floor/FootWasher',
                        'Light/Total', 'Temperature/Total', 'Humidity/Total', 'Power/Total']
        },
        {
            'id': 3,
            'name': 'Corridor',
            'devices': ['Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Light/Total',
                        'Temperature/Total', 'Humidity/Total', 'Power/Total']
        },
        {
            'id': 4,
            'name': 'Kitchen',
            'devices': ['Refrigerator/Temperature', 'Refrigerator/Power', 'Cabinet/Top', 'Cabinet/Bottom',
                        'Light/Total', 'Temperature/Total', 'Humidity/Total', 'Power/Total',
                        'Water/Temperature', 'Water/Amount', 'Oven/Temperature', 'Oven/Power']
        },

        {
            'id': 5,
            'name': 'Office',
            'devices': ['Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Wall/Position', 'Table/Height',
                        'Light/Total', 'Temperature/Total', 'Humidity/Total', 'Power/Total'] #  'Wall/Move'
        },

        {
            'id' : 6,
            'name' : 'Living',
            'devices': ['Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Light/Total', 'Temperature/Total',
                        'Humidity/Total', 'Power/Total']
        },

        {
            'id': 7,
            'name': 'Bedroom',
            'devices': ['Door/Interior', 'Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Wall/Position',
                        'Bed/Position', 'Light/Total', 'Temperature/Total',
                        'Humidity/Total', 'Power/Total']    # 'Wall/Move'
        },
        {
            'id': 8,
            'name': 'Bathroom',
            'devices': ['Door/Interior', 'Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Toilet/Height', 'Cabinet/Height',
                        'Light/Total', 'Temperature/Total', 'Humidity/Total', 'Power/Total']
        },
        {
            'id': 9,
            'name': 'Laundry',
            'devices': ['Flow/Valve', 'Flow/Volume', 'Power/Total']
        },
        {
            'id': 10,
            'name': 'Outside',
            'devices': ['Garden/Light', 'Weather/Temperature', 'Weather/Humidity', 'Weather/WindSpeed',
                        'Weather/Radiation', 'Roof/SolarThermal', 'MechanicalCloset/Power', 'Power/Inverter',
                        'Power/Charger']
        }
    ]
    return rooms

# list of devices in each room
def Devices():
    devices = {
            'Lobby': ['Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Door/Hatch', 'Light/Total'],
            'Washroom': ['Door/Interior', 'Sink/Height', 'Floor/FootWasher', 'Light/Total'],
            'Corridor': ['Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Light/Total'],
            'Kitchen': ['Cabinet/Top', 'Cabinet/Bottom', 'Light/Total'],
            'Office': ['Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Wall/Position', 'Table/Height'],
            'Living': ['Door/Mesh', 'Door/Heavy', 'Door/Thin'],
            'Bedroom': ['Door/Interior', 'Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Wall/Position',
                        'Bed/Position'],
            'Bathroom': ['Door/Interior', 'Door/Mesh', 'Door/Heavy', 'Door/Thin', 'Cabinet/Height'],
            'Laundry': ['Flow/Valve'],
            'Outside': ['Garden/Light']
    }
    return devices

# Corresponding button names for each device
def ButtonName():
    # these are the commands to send to the smart devices
    buttonNames = {
        'Door/Mesh': ['Open', 'Stop', 'Close'],  # corresponding status ['Opened', 'Stopped', 'Closed']
        'Door/Heavy': ['Open', 'Stop', 'Close'],  # corresponding status ['Opened', 'Stopped', 'Closed']
        'Door/Thin': ['Open', 'Stop', 'Close'],   # corresponding status ['Opened', 'Stopped', 'Closed']
        'Door/Hatch': ['Open', 'Stop', 'Close'],  # corresponding status ['Opened', 'Stopped', 'Closed']
        'Light/Total': ['TurnOn', 'TurnOff'], # corresponding status ['On', 'Off']
        'Door/Interior': ['Open', 'Stop', 'Close'], # corresponding status ['Opened', 'Stopped', 'Closed']
        'Sink/Height': ['Up', 'Stop', 'Down'],  # corresponding status ['Up', 'Down', 'Stopped']
        'Floor/FootWasher': ['TurnOn', 'TurnOff'],  # corresponding status ['On', 'Off']

        'Cabinet/Top': ['Up', 'Stop', 'Down'],  # corresponding status ['Up', 'Down', 'Stopped']
        'Cabinet/Bottom': ['Up', 'Stop', 'Down'],  # corresponding status ['Up', 'Down', 'Stopped']

        # send mqtt message with actual "MoveTo[int] %", "Stop"
        'Wall/Position': ['Position1', 'Position2', 'Position3', 'Position4'],
#       'Wall/Move': ['Open', 'Stop', 'Close'],

        'Table/Height': ['Up', 'Stop', 'Down'], # corresponding status ['Up', 'Down', 'Stopped']
        'Bed/Position': ['Up', 'Stop', 'Down'], # corresponding status ['Up', 'Down', 'Stopped']

        'Cabinet/Height': ['Up', 'Stop', 'Down'], # corresponding status ['Up', 'Down', 'Stopped']
        'Flow/Valve': ['TurnOn', 'TurnOff'],  # corresponding status ['On', 'Off']
        'Garden/Light': ['TurnOn', 'TurnOff']  # corresponding status ['On', 'Off']
    }
    return buttonNames

# pi vision dashboard links for each room
def PiVisionLinks():
    piVisionLink = {

        'Lobby': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Washroom': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Corridor': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Kitchen': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Office': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Living': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Bedroom': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Bathroom': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Laundry': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom',
        'Outside': 'https://osisoft/PIVision/#/Displays/3/FutureHAUS_Washroom'
    }
    return piVisionLink

