
"""
This class is created using the following example to include analytics in the webpage
"https://towardsdatascience.com/forecasting-gas-and-electricity-utilization-using-facebook-prophet-558490642f5a".
The aim is to integrate FutureHAUS data with it later.
"""

__author__ = "Archi Dasgupta"
__copyright__ = "Copyright 2018"

import pandas as pd
import numpy as np
import fbprophet as ph
import matplotlib.pyplot as plt
import datetime

foldername = "datasets"
filename = "RefBldgHospitalNew2004_7.1_5.0_3C_USA_CA_SAN_FRANCISCO.csv"
datasource = foldername + '/' + filename
print("datasource: " + datasource)

# read the csv file using pandas library
dataset = pd.read_csv(datasource)

print("dataset shape: ")
print(dataset.shape)

print("dataset head 5 : ")
print(dataset.head(5))

print("dataset tail : ")
print(dataset.tail())

print("dataset columns: ")
print(dataset.columns)

dataset = dataset[['Date/Time', 'Electricity:Facility [kW](Hourly)', 'Gas:Facility [kW](Hourly)']]

df_electric = dataset[['Date/Time', 'Electricity:Facility [kW](Hourly)']]
df_gas = dataset[['Date/Time', 'Gas:Facility [kW](Hourly)']]

# renaming columns for simplicity
df_electric.rename(columns={'Date/Time': 'ds', 'Electricity:Facility [kW](Hourly)': 'y'}, inplace=True)
df_gas.rename(columns={'Date/Time' : 'ds', 'Gas:Facility [kw](Hourly)':'y'}, inplace=True)

print("rename electricity dataset columns: ")
print(df_electric)
print("rename gas dataset columns: ")
print(df_gas)

def modify_date(row):
    return row[:6] + '/2004' + row[6:].replace('24:', '00:')


df_electric['ds'] = df_electric['ds'].apply(modify_date)
df_gas['ds'] = df_gas['ds'].apply(modify_date)

print("df_gas head: ")
print(df_gas['ds'].head())
print("df_gas tail: ")
print(df_gas['ds'].tail())

print('df_electric head: ')
print(df_electric['ds'].head())
print('df_electric tail: ')
print(df_electric['ds'].tail())

print("df_gas plot: ")
df_gas.plot(figsize=(10,5))
#plt.show()

print("df_electric plot: ")
df_electric.plot(figsize=(10,5))
#plt.show()

df_electric['y'] = np.log(df_electric['y'])

print(df_gas['y'])
df_gas['y'] = np.log(df_gas['y'])
print(np.power(np.e, df_gas['y'][0]))
#prohet = ph.Prophet()
#prohet.fit()

