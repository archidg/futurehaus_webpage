// Created by Archi Dasgupta, July 2018

$(document).ready(function() {
    var variable1 = 'http://' + document.domain + ':' + location.port;
    var socket = io.connect(variable1);
    socket.on('connect', function(){
        console.log("connection established");
        console.log('connected to: ' + variable1);

        // Setup button click handlers
        // On clicking the buttons, send mqtt messages ('Turn On / Turn Off') to the broker
        // to control (turn on) the devices in FutureHAUS
        $('.class1').click(function(event) {
            var topic = 'FutureHAUS/' + this.value;   // "FutureHAUS/Lobby/Door/Mesh/Command"

            var str = this.id; // "FutureHAUS/Lobby/Door/Mesh/Open"
            var array = str.split('/');
            var msg = array[array.length-1];
            console.log("COnsole log : message command name: " + msg );

            var qos = '1';
            var data = '{"topic": "' + topic + '", "message": "' + msg + '", "qos": ' + qos + '}';

            socket.emit('publish', data=data);
        });

        // Upon receiving mqtt messages from the broker, toggle the buttons
        // based on device status
        socket.on('mqtt_message', function(data) {
            console.log("MQTT message received via socket: " );
            var mqttTopic = data['topic'];   //"FutureHAUS/Lobby/Light/Total"
            var mqttPayload = data['payload']; //"On"

            // Toggle between On/Off
            if (mqttPayload === 'On'){
                    var buttonId = mqttTopic + "/TurnOn";
                    var buttonId_2 = mqttTopic + "/TurnOff";
            }
            else if (mqttPayload === 'Off') {
                    var buttonId = mqttTopic + "/TurnOff";
                    var buttonId_2 = mqttTopic + "/TurnOn";
            }

            // Toggle between Open/Stop/Close
            else if (mqttPayload === 'Opened'){
                    var buttonId = mqttTopic + "/Open";
                    var buttonId_2 = mqttTopic + "/Close";
                    var buttonId_3 = mqttTopic + "/Stop";
            }
            else if (mqttPayload === 'Closed'){
                    var buttonId = mqttTopic + "/Close";
                    var buttonId_2 = mqttTopic + "/Open";
                    var buttonId_3 = mqttTopic + "/Stop";
            }
            else if (mqttPayload === 'Stopped'){
                    var buttonId = mqttTopic + "/Stop";
                    var buttonId_2 = mqttTopic + "/Open";
                    var buttonId_3 = mqttTopic + "/Close";
            }

            // Toggle between Up/Down/Stop
            else if (mqttPayload === 'Up'){
                    var buttonId = mqttTopic + "/Up";
                    var buttonId_2 = mqttTopic + "/Stop";
                    var buttonId_3 = mqttTopic + "/Down";
            }
            else if (mqttPayload === 'Down'){
                    var buttonId = mqttTopic + "/Down";
                    var buttonId_2 = mqttTopic + "/Stop";
                    var buttonId_3 = mqttTopic + "/Up";
            }
            else if (mqttPayload === 'Stopped'){
                    var buttonId = mqttTopic + "/Stop";
                    var buttonId_2 = mqttTopic + "/Down";
                    var buttonId_3 = mqttTopic + "/Up";
            }

            else if (!isNaN(mqttPayload)){
                    var buttonId = mqttTopic + "/" + mqttPayload;
            }

            console.log('buttonId: ' + buttonId);
            console.log('buttonId_2: ' + buttonId_2);
            console.log('buttonId_3: ' + buttonId_3);  // buttonId_3 is undefined in case of On/Off buttons

            var myButton = document.getElementById(buttonId);
            var myButton_2 = document.getElementById(buttonId_2);
            var myButton_3 = document.getElementById(buttonId_3);
      //      $(myButton).hide();
            $(myButton).attr("disabled","disabled");
      //      $(myButton_2).show();
            $(myButton_2).removeAttr('disabled');
      //      $(myButton_3).show();
            $(myButton_3).removeAttr('disabled');
        })
//*************************
        // This Analytics is based on a house power consumption data from another source
        // Add analytics to the webpage
////        var buttonAnalytics = document.getElementById("forecastHousePowerConsumption");
////
////        $(buttonAnalytics).click(function(event) {
////            $("#image_location").html(response.message);
////
////        });
//************************

    })
});